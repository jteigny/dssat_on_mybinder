# dssat_export_test_scritps


root :
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fjteigny%2Fdssat_on_mybinder/HEAD)

jupyter notebook "test_init" :
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fjteigny%2Fdssat_on_mybinder/HEAD?labpath=test_init.ipynb)
 
jupyter notebook "Tuto_first_step" :
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fjteigny%2Fdssat_on_mybinder/HEAD?labpath=Tuto_first_step.ipynb)
 
